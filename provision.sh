#!/bin/bash - 
#===============================================================================
#
#          FILE: provision.sh
# 
#         USAGE: ./provision.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 31/10/2017 13:37
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error


apt install apache2 php libapache2-mod-php tmux mysql-server
apt install php php-bcmath php-ctype php-curl php-gd php-iconv php-json php-mbstring php-mcrypt php-mysqli php-pdo_dblib php-pdo_mysql php-pdo_sqlite php-phar php-zip
