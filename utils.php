<?php


function generate_crypto($link){
    function base16Encode($arg){
        $ret="";
        for($i=0;$i<strlen($arg);$i++){
            $tmp=ord(substr($arg,$i,1));
            $ret.=dechex($tmp);
        }
        return $ret;
    }

    $key="";
    for( $i = 0; $i <16; $i++){
        $key .= mt_rand(0,9);
    }
    $transmitKey=base16Encode($key);
    $cp = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
    @mcrypt_generic_init($cp, $key,$key);
    $enc = mcrypt_generic($cp, $link);
    mcrypt_generic_deinit($cp);
    mcrypt_module_close($cp);
    $crypted=base64_encode($enc);
    return [$crypted,$transmitKey];
}
