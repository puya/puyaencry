<?php

require 'vendor/autoload.php';
use \RedBeanPHP\R as R;
use Hashids\Hashids;

define("DBTABLE", "entries");

require 'conf.php';
require 'utils.php';

R::setup( 'mysql:host=localhost;dbname='.DBNAME, DBUSER, DBPASS);
Flight::set('flight.log_errors', true);

Flight::route("/admin/api/",function(){

    session_start();
    if( !isset( $_SESSION["login"] ) ) {
        Flight::stop();
    }

    $req = Flight::request();
    $action = $req->query->action;
    if( $action == "list") {
        $results = R::find(DBTABLE, "order by id desc");
        $entries = R::exportAll($results);
        Flight::json($entries);
    } else if( $action == "delete") {
        $id = $req->query->id;
        $entry = R::load(DBTABLE, $id );
        R::trash($entry);
    }
});


Flight::route("/admin",function(){
    session_start();
    Flight::render("_header", array('title'=>TITLE, 'GTAG'=>GTAG), "header");
    Flight::render("_footer", array(), "footer");


    $req = Flight::request();
    if( $req->data->santoysenia == URLPASS) {
        $_SESSION["login"] = True;
    }

    if ( isset($_SESSION["login"]) ) {
        $context = ["URLPASS"=>URLPASS];
        Flight::render("admin", $context);
    } else {
        $context = ["URLPASS"=>""];
        Flight::render("login", $context);
    }
});

Flight::route('/add/', function(){
    $req = Flight::request();
    $pass = $req->data->santoysenia;
    $url = $req->data->url;
    if( is_null($pass) ) {

        $pass = $req->query->santoysenia;
        $url = $req->query->url;
    }
    if($pass != URLPASS ) {
        echo "wtf!";
        Flight::stop();
    }
    $hashids = new Hashids(URLPASS);
    $entry = R::dispense(DBTABLE);
    $entry->url = $url;
    $id = R::store($entry);
    $id_e = $hashids->encode($id);
    Flight::json(array('id' => $id_e));
});

Flight::route('/', function(){
    Flight::render("_header", array('title'=>TITLE, 'GTAG'=>GTAG), "header");
    Flight::render("_footer", array(), "footer");


    $hashids = new Hashids(URLPASS);
    $req = Flight::request();
    $id_e = $req->query->id;
    if(is_null($id_e)){
        Flight::render("noindex");
        Flight::stop();
    }
    $id = $hashids->decode($id_e);
    $link = R::load(DBTABLE,array_pop($id));
    list($crypted, $key) = generate_crypto($link->url);
    $context = array('crypted'=>$crypted,'title'=>TITLE,  'key'=>$key, 'id'=>$id_e);
    Flight::render("index", $context);
    Flight::stop();
});
Flight::start();

