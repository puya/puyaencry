var Vue = require("vue/dist/vue.js");
var axios = require("axios");
var VueAxios = require("vue-axios");
var GoodTable  = require("vue-good-table/dist/vue-good-table.js");
var HashIds = require("hashids");


Vue.use(VueAxios, axios);
hashids = new HashIds(HPASS);


new Vue({
    el:'#app',
    components: { 'vue-good-table' : GoodTable.VueGoodTable },
    data: {
        new_url:"",
        columns:[
            {label:'id', field:'id'},
            {label:'url', field:'url'},
            {label:'Encrypted', field: 'id'},
            {label:'Actions', field: 'id'}
        ],
        list:[]
    },
    methods:{

        decode_url: function(id){
            var id_resp = hashids.encode(id);
            return "/?id=" +id_resp;
        },
        decode: function(id){
            var id_resp = hashids.encode(id);
            return id_resp;
        },
        refresh_list: function(){
            var self = this;
            Vue.axios.get(URLBASE + "admin/api/",{params:{action:'list'}}).then(function(response) {
                self.list = response.data;
            });
        },
        add_link: function(){
            var self = this;
            Vue.axios.post(URLBASE +"add/", {url:this.new_url, santoysenia:HPASS}).then(function (){
                self.refresh_list();
            });
        },
        click_delete: function(id) {
            console.log("delete", id);
            var self = this;
            Vue.axios.get(URLBASE + "admin/api/", {
                    params:{action:'delete', id:id}
            }).then(function(resp){
                self.refresh_list();
            });
        }

    },
    mounted:function (){
        this.refresh_list();
    }

});
