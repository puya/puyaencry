// See http://brunch.io for documentation.
exports.files = {
  javascripts: {joinTo: 'app.js'},
  stylesheets: {joinTo: 'app.css'},
};

exports.plugins= {
    browserSync: {
        port:3333,
        logLevel: "debug",
        proxy:'localhost:8000',
        startPath: '/admin',
        reloadDelay: 2000,
        reloadDebounce: 2000,
        logFileChanges: true,
        files: [
            'app/**/*.*',
            'views/**/*.*',
            '*.php'
        ],
        serverStatic: ["/public/**.*"]
    },
    uglify:{ 
        compress: {
            global_defs: {
              DEBUG: false
            }
        },
        mangle: false
    }
};

