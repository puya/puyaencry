Encryptador de puya
===================

La razon es que safelinking no es muy estable usando su api, se crean los links pero estos desaparecen al poco tiempo.


![Jdownloader & admin](https://i.imgur.com/xsjJJm8.gif)


Este script es la implementación de Click n Load de jdownloader para remplazar el safelinking.net


Echo con [flight](http://flightphp.com/)/[vuejs](https://vuejs.org)/[RedBeanPHP](https://redbeanphp.com)


para Desarrollo  es necesario instalar

    composer install 
    yarn install
    pip install honcho
    honcho start


Php deps:

    php php-bcmath php-ctype php-curl php-gd php-iconv php-json php-mbstring php-mcrypt php-mysqli php-pdo_dblib php-pdo_mysql php-pdo_sqlite php-phar php-zip

    for ubuntu:
    apt install php php-bcmath php-ctype php-curl php-gd php-iconv php-json php-mbstring php-mcrypt php-mysqli php-pdo_dblib php-pdo_mysql php-pdo_sqlite php-phar php-zip


Agregar una url nueva:

    curl --data 'santoysenia=pass&url=url' localhost:8000/add/

Administración:

    http://localhost:8000/admin/


Instalación:

    cp conf.php.ini conf.php # editar conf.php


Configuración de apache/nginx como dominio/subdominio:

[install](http://flightphp.com/install/)

Configuración de NGINX en directorio:


        location  /enc {

                alias /root/puyaencry/;
                index index.php ;

                try_files $uri $uri/ /enc/index.php;


                location ~ \.php$ {
                        include fastcgi_params;
                        fastcgi_index index.php;
                        fastcgi_pass 127.0.0.1:8080;

                }
                error_log /var/log/nginx/enc.log info;
                access_log /var/log/nginx/enc.log;
        }


