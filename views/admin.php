<?=$header?>
  <section class="section">

    <h1 class="title">Lista</h1>
    <div class="container" id="app">


    <div class="field has-addons">
        <p class="control is-expanded">
            <input class="input is-expanded" type="text" v-model="new_url" placeholder="URL">
        </p>
        <p class="control">
            <a class="button" @click.prevent="add_link"> Agregar</a>
        </p>
    </div>


     <vue-good-table
          title=""
          :columns="columns"
          :rows="list"
          :paginate="true"
          :lineNumbers="true">
     <template slot="table-row" scope="props">
        <td class="fancy">{{ props.row.id }}</td>
        <td class="has-text-right">
            {{ props.row.url }}
        </td>

        <td class="has-text-right">
        <a v-bind:href="'<?=URLBASE?>?id=' + decode(props.row.id)">
                {{ decode(props.row.id) }}
            </a>
        </td>
        <td>
          <a class="button is-small" @click="click_delete(props.row.id)">delete</a>
        </td>
      </template>
    </vue-good-table>
    </div>
        <script src="<?=URLBASE?>/public/app.js"></script>
    <script>
    var HPASS = "<?=$URLPASS?>";
    require("admin");
    </script>

  </section>
</body>
