<?=$header?>

  <section class="section is-large" id="app" data-background-image="https://i.imgur.com/pRkroWf.png">
    <div class="container" >
<div class="columns">
    <div class="column notification is-half is-offset-one-quarter has-text-centered">
    <h1 class="title has-text-centered"> <?=$title?> </h1>
        <div style="display:none;" v-show="jdownloader_alive == false">

            <div class="notification is-danger">
            Detectamos que no tienes ejecutando el jdownloader por favor abrelo.
            </div>
        </div>

        <div class="has-text-centered">
        <h2 class="subtitle">CNL2</h2> 
        <FORM ACTION="http://127.0.0.1:9666/flash/addcrypted2" METHOD="POST">
           <INPUT TYPE="hidden" NAME="source" VALUE="http://localhost:8000">
           <INPUT TYPE="hidden" NAME="jk" VALUE="function f(){ return '<?=$key?>';}">
           <INPUT TYPE="hidden" NAME="crypted" VALUE="<?=$crypted?>">
           <button class="button is-primary is-large" TYPE="SUBMIT" NAME="submit" onclick="gtag('event','encrypt','<?=$id?>');">
                    <img src="public/jdown.png">
                    Agregar al JDownloader
            </button>
        </FORM>
        </div>

        </div>

    </div>
</div>

  </section>
<script>
    var jdownloader=false;
</script>
  <script language="javascript" src="http://127.0.0.1:9666/jdcheck.js"></script>
  <script src="public/app.js"></script>
  <script>require('initialize');</script>

<?=$footer?>
